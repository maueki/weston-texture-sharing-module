
#include <string>
#include <fstream>
#include <vector>
#include <cassert>

#include <weston.h>

#include "texture-sharing-server-protocol.h"

#define LOG_PREFIX "texture-sharing: "

/// Note: linux-dmabuf.h からのコピペ
#define MAX_DMABUF_PLANES 4
#ifndef DRM_FORMAT_MOD_INVALID
#define DRM_FORMAT_MOD_INVALID ((1ULL<<56) - 1)
#endif
#ifndef DRM_FORMAT_MOD_LINEAR
#define DRM_FORMAT_MOD_LINEAR 0
#endif

struct linux_dmabuf_buffer;
typedef void (*dmabuf_user_data_destroy_func)(
			struct linux_dmabuf_buffer *buffer);

struct dmabuf_attributes {
	int32_t width;
	int32_t height;
	uint32_t format;
	uint32_t flags; /* enum zlinux_buffer_params_flags */
	int n_planes;
	int fd[MAX_DMABUF_PLANES];
	uint32_t offset[MAX_DMABUF_PLANES];
	uint32_t stride[MAX_DMABUF_PLANES];
	uint64_t modifier[MAX_DMABUF_PLANES];
};

struct linux_dmabuf_buffer {
	struct wl_resource *buffer_resource;
	struct wl_resource *params_resource;
	struct weston_compositor *compositor;
	struct dmabuf_attributes attributes;

	void *user_data;
	dmabuf_user_data_destroy_func user_data_destroy_func;

	/* XXX:
	 *
	 * Add backend private data. This would be for the backend
	 * to do all additional imports it might ever use in advance.
	 * The basic principle, even if not implemented in drivers today,
	 * is that dmabufs are first attached, but the actual allocation
	 * is deferred to first use. This would allow the exporter and all
	 * attachers to agree on how to allocate.
	 *
	 * The DRM backend would use this to create drmFBs for each
	 * dmabuf_buffer, just in case at some point it would become
	 * feasible to scan it out directly. This would improve the
	 * possibilities to successfully scan out, avoiding compositing.
	 */

	/**< marked as scan-out capable, avoids any composition */
	bool direct_display;
};
/// linux-dmabuf.h からのコピペここまで

extern "C"
struct linux_dmabuf_buffer *linux_dmabuf_buffer_get(struct wl_resource *resource);

struct Context;

struct Surface {
    Context* context;
    weston_surface* surface;
    wl_listener destroy_listener;
    wl_listener commit_listener;
};

struct Context {
    wl_listener create_surface_listener;
    wl_resource* texture_sharing_resource = nullptr;
};


static void
send_dmabuf_info(Surface* surface)
{
    weston_buffer *buffer = surface->surface->buffer_ref.buffer;
    if (!buffer) {
        return;
    }

    linux_dmabuf_buffer *dmabuf = linux_dmabuf_buffer_get(buffer->resource);
    if (!dmabuf) {
        return;
    }

    auto n_planes = dmabuf->attributes.n_planes;
    assert(n_planes == 2);

    if (surface->context->texture_sharing_resource) {
        texture_sharing_send_dmabuf_fd(surface->context->texture_sharing_resource,
                                       dmabuf->attributes.width, dmabuf->attributes.height,
                                       dmabuf->attributes.format,
                                       dmabuf->attributes.fd[0],
                                       dmabuf->attributes.offset[0],
                                       dmabuf->attributes.stride[0],
                                       dmabuf->attributes.fd[1],
                                       dmabuf->attributes.offset[1],
                                       dmabuf->attributes.stride[1]);
    }
}

static bool
is_gst_surface(weston_surface* surface)
{
    wl_client* client = wl_resource_get_client(surface->resource);

    pid_t pid;
    wl_client_get_credentials(client, &pid, nullptr, nullptr);

    std::string path = "/proc/" + std::to_string(pid) + "/cmdline";
    std::ifstream file;
    file.open(path, std::ios::in);
    if (!file) {
        weston_log(LOG_PREFIX "failed to open %s\n", path.c_str());
        return false;
    }

    std::vector<char> buf(1024);
    file.read(buf.data(), buf.size());

    std::string cmdline{};
    cmdline.append(buf.data(), file.gcount());

    return cmdline.find("gst-launch") != std::string::npos;
}

static void
ts_surface_destroy(wl_listener *listener, void *data)
{
    weston_log(LOG_PREFIX "surface detroyed\n");
    Surface* surface = wl_container_of(listener, surface, destroy_listener);
}

static void
ts_surface_commit(wl_listener *listener, void *data)
{
    Surface* surface = wl_container_of(listener, surface, commit_listener);
    send_dmabuf_info(surface);
}

static void
ts_create_surface(wl_listener *listener, void *data)
{
    weston_surface* wet_surface = static_cast<weston_surface*>(data);
    Context* context = wl_container_of(listener, context, create_surface_listener);

    if (is_gst_surface(wet_surface)) {
        Surface* surface = new Surface{context, wet_surface};
        surface->destroy_listener.notify = ts_surface_destroy;
        wl_signal_add(&wet_surface->destroy_signal,
                      &surface->destroy_listener);
        surface->commit_listener.notify = ts_surface_commit;
        wl_signal_add(&wet_surface->commit_signal,
                      &surface->commit_listener);
    }
}

static void
unbind_resource(struct wl_resource *resource)
{
    Context* context = reinterpret_cast<Context*>(wl_resource_get_user_data(resource));
    context->texture_sharing_resource = nullptr;

    wl_list_remove(wl_resource_get_link(resource));
}

static void
bind_texture_sharing(wl_client* client, void* data, uint32_t version, uint32_t id)
{
    weston_log(LOG_PREFIX "bind_texture_sharing\n");

    auto context = reinterpret_cast<Context*>(data);
    auto resource = wl_resource_create(client, &texture_sharing_interface, version, id);
    wl_resource_set_implementation(resource, nullptr, context, unbind_resource);

    context->texture_sharing_resource = resource;
}

extern "C"
{

WL_EXPORT int
wet_module_init(struct weston_compositor *compositor,
                int *argc, char* argv[])
{
    Context* context = new Context{};

    context->create_surface_listener.notify = ts_create_surface;
    wl_signal_add(&compositor->create_surface_signal,
                  &context->create_surface_listener);

    if (wl_global_create(compositor->wl_display, &texture_sharing_interface, 1,
                         context, bind_texture_sharing) == nullptr) {
        weston_log(LOG_PREFIX "wl_global_create failed\n");
        return -1;
    }

    return 0;
}

}
